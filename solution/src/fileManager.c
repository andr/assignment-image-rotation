#include "fileManager.h"
enum stateFile fileOperation(FILE **file, const char *argv, char *type) {
    *file = fopen(argv, type);
    if (*file == NULL) {
        switch (*type) {
            case 'r':  return FILEREAD_ERROR; break;
            case 'w':  return FILEWRITE_ERROR; break;
        }
    }
    return FILE_OK;




}

