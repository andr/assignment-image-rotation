#include "transforms/transform.h"

struct image transform( struct image const source, struct image (*func)(struct image const *input, struct image *output)) {
    struct image output = imageStructCreate(source.width, source.height);
    return func(&source, &output);
}


