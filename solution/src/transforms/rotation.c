#include "transforms/rotation.h"

struct image rotate( struct image const *input, struct image *output ) {

    for (uint64_t i = 0; i < input->width; i++) {
        for (uint64_t j = 0; j < input->height; j++) {
            output->data[j + i * input->height ]  = input->data[i + (input->height-1-j) * input->width ];
        }
    }
    output->width = input->height;
    output->height = input->width;
    return *output;
}


