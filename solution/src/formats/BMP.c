#include <formats/BMP.h>

#define TYPE 19778
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24

struct bmp_header;

enum stateFile from_bmp( FILE* in, struct image* img ){


    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    if (!fread(header, sizeof(struct bmp_header), 1, in))
        return READ_ERROR;

    *img = imageStructCreate(header->biWidth, header->biHeight);

    for(uint32_t i=0; i< header->biHeight; i++) {
        fread(&(img->data[i*img->width]), sizeof(struct pixel), header->biWidth, in);
        fseek( in, header->biWidth%4, 1);
    }

    free(header);
    return fclose(in) == EOF?CLOSE_ERROR:FILE_OK;
}



enum stateFile to_bmp( FILE* out, struct image const* img ) {

    struct bmp_header header = {
            .bfType = TYPE,
            .bfileSize = img->height * img->width * sizeof(struct pixel)
                                  + (img->width % 4)*img->height * sizeof(struct pixel),
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biSizeImage =
            img->height * img->width * sizeof(struct pixel)
            + (img->width % 4)*img->height
    };

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    if(img->data==NULL)  return WRITE_ERROR;

    uint32_t padding = 0;
    for(uint32_t r=0; r< img->height; r++) {
        fwrite(&(img->data[r*img->width]),sizeof(struct pixel), img->width, out);
        fwrite(&padding, 1, img->width % 4, out);
    }



    free(img->data);
    return fclose(out) == EOF? CLOSE_ERROR:FILE_OK;
}
