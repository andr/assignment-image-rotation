#include "formats/BMP.h"
#include "image.h"
#include "transforms/rotation.h"
#include "transforms/transform.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if(argc!=3) return 1;

    struct image img = openImageFile(argv[1], from_bmp);
    struct image newImg = transform(img, rotate);
    createImageFile(&newImg, to_bmp, argv[2]);

    free(img.data);

    return 0;
}


