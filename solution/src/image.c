#include "image.h"

struct image;

struct image imageStructCreate(const size_t width, const size_t height) {
    return ((struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    });
}

struct image openImageFile(char *argv, enum stateFile (*f)( FILE* file, struct image* img )) {
    FILE *file = {0};
    struct image img;
    fileOperation(&file, argv, "r");
    f(file, &img);
    return img;
}
void createImageFile(struct image *img, enum stateFile (*f)(FILE* file, struct image const* img), char *argv) {
    FILE *file = {0};
    fileOperation(&file, argv, "w");
    f(file, img);
}
