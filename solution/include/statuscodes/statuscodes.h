#ifndef ASSIGNMENT_IMAGE_ROTATIONNORM_STATUSCODES_H
#define ASSIGNMENT_IMAGE_ROTATIONNORM_STATUSCODES_H
enum  rotate_status  {
    ROTATE_OK = 0,
    ROTATE_ERROR,

};
enum stateImage  {
    IMAGE_OK = 0,
};
enum stateFile  {
    FILE_OK = 0,
    OPENFILE_ERROR,
    CREATEFILE_ERROR,
    READ_ERROR,
    CLOSE_ERROR,
    WRITE_ERROR,
    FILEWRITE_ERROR,
    FILEREAD_ERROR,



};

#endif //ASSIGNMENT_IMAGE_ROTATIONNORM_STATUSCODES_H
