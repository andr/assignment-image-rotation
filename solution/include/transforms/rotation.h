//
// Created by leons on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "../image.h"

struct image rotate( struct image const *input, struct image *output );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
