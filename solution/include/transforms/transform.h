
#ifndef ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
#include "../image.h"
struct image transform( struct image const source, struct image (*func)(struct image const *input, struct image *output));
#endif //ASSIGNMENT_IMAGE_ROTATION000000_TRANSFORM_H
