#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include "fileManager.h"
#include <stdlib.h>
#include <stdio.h>
#include "statuscodes/statuscodes.h"
#include <stdint.h>


struct pixel { uint8_t b, g, r; };


struct image {
    uint64_t width, height;
    struct pixel* data;
};


struct image imageStructCreate(size_t width, size_t height);
struct image openImageFile(char *argv, enum stateFile (*f)( FILE* file, struct image* img ));
void createImageFile(struct image *img, enum stateFile (*f)(FILE* file, struct image const* img), char *argv);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
